#  mayiktCloudConfig

#### 介绍
 mayiktCloudConfig 分布式配置中心 可以实现不用重启修改服务器配置文件内容，实时动态刷新。 
 支持可视化界面管理配置文件内容。   **关注微信公众号 架构师余胜军 回复"88" ，可以直接获取该项目最新源码** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/155804_ef4698fa_926394.png "屏幕截图.png")
#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/155823_af838646_926394.png "屏幕截图.png")


#### 安装教程

0. 初始化表结构


```
CREATE TABLE `mayikt_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `md5` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
CREATE TABLE `mayikt_config_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `config_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
```


1. Maven依赖：

```
<dependency>
            <groupId>com.mayikt</groupId>
            <artifactId>mayikt-cloud-config-client-spring-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
```

2.  配置文件新增

```
mayikt:
  cloud:
    server-config-url: http://127.0.0.1:8099/
spring:
  application:
    name: mayikt-member

```

3.  项目配置读取配置文件

```
  @Value("${mayikt.name}")
    private String name;
    @Value("${mayikt.address}")
    private String address;
```
可以实现不用重启修改服务器配置文件内容，实时动态刷新。 
#### 演示效果
1. 当前读取到配置配置内容：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/160149_e02ce237_926394.png "屏幕截图.png")
2. 动态修改配置文件内容
![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/160233_0e54c79a_926394.png "屏幕截图.png")
3.不需要重启服务器即可刷新配置文件
![输入图片说明](https://images.gitee.com/uploads/images/2021/1113/160255_55f4ef2d_926394.png "屏幕截图.png")